package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class VersementServiceTest {


    @Autowired
    VersementService versementService;
    @Autowired
    CompteRepository compteRepository;
    @Autowired
    VersementRepository versementRepository;

    Compte compteBeneficiaire;

    @BeforeEach
    void beforeEach() {

        compteBeneficiaire = new Compte();
        compteBeneficiaire.setNrCompte("123456789");
        compteBeneficiaire.setRib("RIB123456789");
        compteBeneficiaire.setSolde(BigDecimal.valueOf(500));
        compteBeneficiaire = compteRepository.save(compteBeneficiaire);

    }

    @AfterEach
    void afterAll() {
        compteRepository.deleteById(compteBeneficiaire.getId());
    }

    @Test
    void allVersements() {
        List<Versement> versements = versementService.allVersements();
        assertNotNull(versements);
        assertNotEquals(versements.size(), 0);
    }

    @Test
    void createTransactionCompteNotExist() {
        var versementDto = new VersementDto();

        versementDto.setMontantVersement(new BigDecimal(300));
        versementDto.setMotif("Avance");
        versementDto.setRib("Not Excite");
        versementDto.setNom_prenom_emetteur("Basma Houmia");

        assertThrows(CompteNonExistantException.class, () -> versementService
                .createTransactionVersement(versementDto));
    }

    @Test
    void createTransactionMontanteInValid() {

        var versementDto = new VersementDto();

        versementDto.setMontantVersement(new BigDecimal(-300));
        versementDto.setMotif("Avance");
        versementDto.setRib("RIB123456789");
        versementDto.setNom_prenom_emetteur("Basma Houmia");

        assertThrows(TransactionException.class, () -> versementService
                .createTransactionVersement(versementDto));

    }

    @Test
    void createTransactionValid() throws TransactionException, CompteNonExistantException {

        var versementDto = new VersementDto();
        BigDecimal montant = BigDecimal.valueOf(3000);
        String nomEtpernome = "Basma Houmia";
        String motif = "Avance";
        String rib = "RIB123456789";

        versementDto.setMontantVersement(montant);
        versementDto.setMotif(motif);
        versementDto.setRib(rib);
        versementDto.setNom_prenom_emetteur(nomEtpernome);
        Versement versement = versementService
                .createTransactionVersement(versementDto);

        assertNotNull(versement);
        assertEquals(rib, versement.getCompteBeneficiaire().getRib());
        assertEquals(montant, versement.getMontantVirement());
        assertEquals(nomEtpernome, versement.getNom_prenom_emetteur());
        assertEquals(motif, versement.getMotifVersement());

        assertDoesNotThrow(() -> versementRepository.deleteById(versement.getId()));
    }
}