package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class VirementServiceTest {

    @Autowired
    VirementService virementService;
    @Autowired
    CompteRepository compteRepository;
    @Autowired
    VirementRepository virementRepository;
    @Autowired
    UtilisateurRepository utilisateurRepository;

    Compte compteBeneficiaire;
    Compte compteEmetteur;

    @BeforeEach
    void beforeEach() {

        compteBeneficiaire = new Compte();
        compteBeneficiaire.setNrCompte("123456789");
        compteBeneficiaire.setRib("RIB123456789");
        compteBeneficiaire.setSolde(BigDecimal.valueOf(500));
        compteBeneficiaire = compteRepository.save(compteBeneficiaire);

        compteEmetteur = new Compte();
        compteEmetteur.setNrCompte("987654321");
        compteEmetteur.setRib("RIB987654321");
        compteEmetteur.setSolde(BigDecimal.valueOf(500));
        compteEmetteur = compteRepository.save(compteEmetteur);

    }

    @AfterEach
    void afterAll() {
        compteRepository.deleteById(compteBeneficiaire.getId());
        compteRepository.deleteById(compteEmetteur.getId());
    }

    @Test
    void allVirements() {
        List<Virement> virements = virementService.allVirements();
        assertNotNull(virements);
        assertNotEquals(virements.size(), 0);
    }

    @Test
    void createTransactionCompteNotExist() {
        VirementDto virementDto = new VirementDto();

        virementDto.setMontantVirement(new BigDecimal(300));
        virementDto.setMotif("Avance");
        virementDto.setNrCompteBeneficiaire("Not Excite");
        virementDto.setNrCompteEmetteur("Not Excite");

        assertThrows(CompteNonExistantException.class, () -> virementService
                .createTransaction(virementDto));
    }

    @Test
    void createTransactionSoldInsuffisant() {

        VirementDto virementDto = new VirementDto();

        virementDto.setMontantVirement(new BigDecimal(3000));
        virementDto.setMotif("Avance");
        virementDto.setNrCompteBeneficiaire("123456789");
        virementDto.setNrCompteEmetteur("987654321");

        assertThrows(SoldeDisponibleInsuffisantException.class, () -> virementService
                .createTransaction(virementDto));
    }

    @Test
    void createTransactionMontanteInValid() {

        VirementDto virementDto = new VirementDto();

        virementDto.setMontantVirement(BigDecimal.valueOf(-200));
        virementDto.setMotif("Avance");
        virementDto.setNrCompteBeneficiaire("123456789");
        virementDto.setNrCompteEmetteur("987654321");

        assertThrows(TransactionException.class, () -> virementService
                .createTransaction(virementDto));

    }

    @Test
    void createTransactionMontanteValid() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

        VirementDto virementDto = new VirementDto();

        virementDto.setMontantVirement(BigDecimal.valueOf(200));
        virementDto.setMotif("Avance");
        virementDto.setNrCompteEmetteur("987654321");
        virementDto.setNrCompteBeneficiaire("123456789");

        BigDecimal compteEmetteurSoldBefore = compteRepository.findByNrCompte("987654321").getSolde();
        BigDecimal compteBeneficiaireSoldBefore = compteRepository.findByNrCompte("123456789").getSolde();

        Virement transaction = virementService
                .createTransaction(virementDto);


        assertNotNull(transaction);

        assertEquals(virementDto.getNrCompteBeneficiaire(),
                transaction.getCompteBeneficiaire().getNrCompte());

        assertEquals(virementDto.getNrCompteEmetteur(),
                transaction.getCompteEmetteur().getNrCompte());

        assertEquals(virementDto.getMontantVirement(), transaction.getMontantVirement());


        assertEquals(virementDto.getMotif(), transaction.getMotifVirement());

        assertEquals(compteEmetteurSoldBefore.subtract(virementDto.getMontantVirement()),
                transaction.getCompteEmetteur().getSolde());

        assertEquals(compteBeneficiaireSoldBefore.add(virementDto.getMontantVirement()),
                transaction.getCompteBeneficiaire().getSolde());

        assertDoesNotThrow(() -> virementRepository.deleteById(transaction.getId()));
    }
}