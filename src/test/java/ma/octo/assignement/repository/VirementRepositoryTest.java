package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Virement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

    @Autowired
    private VirementRepository virementRepository;

    @Test
    public void findOne() {
        /*Example<Virement> example = Example.of();
        virementRepository.findOne();*/
    }

    @Test
    public void findAll() {
        List<Virement> all = virementRepository.findAll();
        Assertions.assertNotNull(all);
        Assertions.assertNotEquals(all.size(), 0);
    }

    @Test
    public void save() {
        Virement virement = new Virement();
        virement.setMontantVirement(new BigDecimal(100));
        Virement save = virementRepository.save(virement);
        Assertions.assertNotNull(save);
        Assertions.assertEquals(save.getMontantVirement(), new BigDecimal(100));
    }

    @Test
    public void delete() {
        Virement virement = new Virement();
        virement.setMontantVirement(new BigDecimal(100));
        Virement save = virementRepository.save(virement);
        virementRepository.delete(save);
        Optional<Virement> byId = virementRepository.findById(save.getId());
        Assertions.assertThrows(NoSuchElementException.class, () -> byId.get());
        Assertions.assertFalse(byId.isPresent());
    }
}