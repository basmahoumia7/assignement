package ma.octo.assignement.web;


import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.VirementService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import java.util.List;
@RequestMapping("/virements")
@RestController(value = "/virements")
class VirementController {

	Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

	@Autowired
	private VirementService VirementService;
	@Autowired
	private VirementMapper virementMapper;

	@GetMapping("/lister_virements")
	public List<VirementDto> loadAllVirement() {
		return virementMapper.map(VirementService.allVirements());
				
	}

	@PostMapping("/executerVirements") 
	@ResponseStatus(HttpStatus.CREATED)
	public void createTransactionVirementDto(@RequestBody VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		
		     VirementService.createTransaction(virementDto);

	}

}
