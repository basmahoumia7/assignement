package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;

@RestController(value = "/utilisateur")
public class UtilisateurController {
	@Autowired
	 private UtilisateurService  utilisateurService;
	
	
	
	  @GetMapping("lister_utilisateurs")
	   public List<Utilisateur> allUtilisateur() {
	      return  utilisateurService.loadAllUtilisateur();

	  }

}
