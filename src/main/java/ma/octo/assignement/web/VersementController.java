package ma.octo.assignement.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersememtMapper;
import ma.octo.assignement.service.VersementService;

@RequestMapping("/versement")
@RestController(value = "/versement")
public class VersementController {
	
	Logger LOGGER = LoggerFactory.getLogger(VersementController.class);
	
	@Autowired
	private VersementService versementService;
	@Autowired
	private VersememtMapper versememtMapper;

	@GetMapping("/lister_versements")
	public List<VersementDto> loadAllVerement() {

		return versememtMapper.map(versementService.allVersements());

	}

	@PostMapping("/executerVersements")
	@ResponseStatus(HttpStatus.CREATED)
	public void createTransactionVersement(@RequestBody VersementDto versementDto)
			throws CompteNonExistantException, TransactionException {
		versementService.createTransactionVersement(versementDto);

	}

}
