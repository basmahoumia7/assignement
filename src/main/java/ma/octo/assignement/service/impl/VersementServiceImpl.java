package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
public class VersementServiceImpl implements VersementService {

    Logger LOGGER = LoggerFactory.getLogger(VersementServiceImpl.class);
    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AutiService autiService;

    @Override
    public List<Versement> allVersements() {
        List<Versement> allVersement = versementRepository.findAll();

        if (CollectionUtils.isEmpty(allVersement)) {
            return null;
        } else {
            return allVersement;
        }
    }

    private void verificationMontantVersement(VersementDto versementDto) throws TransactionException {
        if (versementDto.getMontantVersement() == null ||
                versementDto.getMontantVersement().intValue() == 0) {
            LOGGER.error("Montant versement est  vide");
            throw new TransactionException("Montant versement est  vide");
        } else if (versementDto.getMontantVersement().intValue() < 10) {
            LOGGER.error("Montant minimal de versement non atteint");
            throw new TransactionException("Montant minimal de versement non atteint");
        }
    }

    private void verificationMotifVersement(VersementDto versementDto) throws TransactionException {
        if (versementDto.getMotif() == null || versementDto.getMotif().isEmpty()) {
            LOGGER.error("Motif versement est vide");
            throw new TransactionException("Motif versement est vide");
        }
    }

    public Versement saveVersement(VersementDto versementDto, String nom_prenom_emetteur, Compte compteBeneficiaire) {
        Versement versement = new Versement();
        versement.setDateExecution(new Date());
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setNom_prenom_emetteur(nom_prenom_emetteur);
        versement.setMontantVirement(versementDto.getMontantVersement());
        versement.setMotifVersement(versementDto.getMotif());
        return versementRepository.save(versement);
    }

    @Override
    public Versement createTransactionVersement(VersementDto versementDto)
            throws CompteNonExistantException, TransactionException {

        String nom_prenom_emetteur = versementDto.getNom_prenom_emetteur();

        Compte compteBeneficiaire = compteService.findByRib(versementDto.getRib());

        verificationMontantVersement(versementDto);

        verificationMotifVersement(versementDto);

        compteService.modifierCompteBeneficiaire(compteBeneficiaire, versementDto.getMontantVersement());

        Versement versement = saveVersement(versementDto, nom_prenom_emetteur, compteBeneficiaire);

        autiService.auditVirement("Versement depuis " + versementDto.getNom_prenom_emetteur() + " vers "
                + versementDto.getRib() + " d'un montant de "
                + versementDto.getMontantVersement().toString());
        return versement;

    }
}
