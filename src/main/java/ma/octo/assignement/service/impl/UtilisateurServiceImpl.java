package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public List<Utilisateur> loadAllUtilisateur() {
        List<Utilisateur> allUtilisateurs = utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(allUtilisateurs)) {
            return null;
        } else {
            return allUtilisateurs;
        }
    }

    @Override
    public Utilisateur saveUtilisateur(String userName, String lastName, String firstName, String gender) {
        if (userName == null || userName.isEmpty()) {
            return null;
        }
        Optional<Utilisateur> oneByUsername = utilisateurRepository.findOneByUsername(userName);
        if (oneByUsername.isPresent()) {
            // should throw exception already excite
            return null;
        }

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername(userName);
        utilisateur1.setLastname(lastName);
        utilisateur1.setFirstname(firstName);
        utilisateur1.setGender(gender);

        return utilisateurRepository.save(utilisateur1);
    }
}


