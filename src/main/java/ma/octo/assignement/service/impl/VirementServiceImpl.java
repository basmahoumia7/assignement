package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
public class VirementServiceImpl implements VirementService {


    Logger LOGGER = LoggerFactory.getLogger(VirementServiceImpl.class);
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AutiService autiService;

    @Override
    public List<Virement> allVirements() {
        List<Virement> allVirement = virementRepository.findAll();

        if (CollectionUtils.isEmpty(allVirement)) {
            return null;
        } else {
            return allVirement;
        }
    }

    public void verificationMontantVirement(VirementDto virementDto) throws TransactionException {
        if (virementDto.getMontantVirement() == null || virementDto.getMontantVirement().intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() < 10) {
            LOGGER.error("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }
    }

    public void verificationMotif(VirementDto virementDto) throws TransactionException {

        if (virementDto.getMotif().isEmpty()) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }
    }

    public void verificationSold(VirementDto virementDto, Compte compte) throws SoldeDisponibleInsuffisantException {

        if (compte.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {

            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour lutilisateur");
        }
    }

    public Virement saveVirement(VirementDto virementDto, Compte compteEmeteur, Compte compteBeneficiaire) {

        Virement virement = new Virement();
        virement.setDateExecution(new Date());
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmeteur);
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());

        return virementRepository.save(virement);
    }

    private void saveVirement(Virement Virement) {
        virementRepository.save(Virement);
    }

    @Override
    public Virement createTransaction(VirementDto virementDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {

        Compte compteEmeteur = compteService.findByNumeroCompte(virementDto.getNrCompteEmetteur());

        Compte compteBeneficiaire = compteService.findByNumeroCompte(virementDto.getNrCompteBeneficiaire());

        verificationMontantVirement(virementDto);

        verificationMotif(virementDto);

        verificationSold(virementDto, compteEmeteur);

        compteService.modifierCompteEmetteure(compteEmeteur, virementDto.getMontantVirement());

        compteService.modifierCompteBeneficiaire(compteBeneficiaire, virementDto.getMontantVirement());

        Virement virement = saveVirement(virementDto, compteEmeteur, compteBeneficiaire);

        autiService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers "
                + virementDto.getNrCompteBeneficiaire() + " d'un montant de "
                + virementDto.getMontantVirement().toString());
        return virement;
    }

}
