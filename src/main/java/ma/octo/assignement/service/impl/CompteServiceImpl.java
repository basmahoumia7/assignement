package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.CompteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CompteServiceImpl implements CompteService {
    Logger LOGGER = LoggerFactory.getLogger(CompteServiceImpl.class);
    @Autowired
    private CompteRepository compteRepository;

    @Override
    public List<Compte> loadAllCompte() {
        List<Compte> allcompte = compteRepository.findAll();

        if (CollectionUtils.isEmpty(allcompte)) {
            return null;
        } else {
            return allcompte;
        }
    }

    @Override
    public Compte findByNumeroCompte(String nrCompte) throws CompteNonExistantException {
        Compte ByNrCompte = compteRepository.findByNrCompte(nrCompte);
        if (ByNrCompte == null) {
            LOGGER.error("compte non exist");
            throw new CompteNonExistantException("compte non exist");
        }
        return ByNrCompte;
    }

    @Override
    public Compte findByRib(String rib) throws CompteNonExistantException {
        Compte ByRib = compteRepository.findByRib(rib);
        if (ByRib == null) {
            LOGGER.error("compte non exist");
            throw new CompteNonExistantException("compte non exist");
        }
        return ByRib;
    }

    @Override
    public void modifierCompteEmetteure(Compte compteEmetteur, BigDecimal montant) {
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montant));
        compteRepository.save(compteEmetteur);
    }


    @Override
    public void modifierCompteBeneficiaire(Compte CompteBeneficiaire, BigDecimal montant) {
        CompteBeneficiaire.setSolde(
                new BigDecimal(CompteBeneficiaire.getSolde().intValue() + montant.intValue()));
        compteRepository.save(CompteBeneficiaire);
    }
}