package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import java.math.BigDecimal;
import java.util.List;

public interface CompteService {

    List<Compte> loadAllCompte();

    Compte findByNumeroCompte(String nrCompte) throws CompteNonExistantException;

    Compte findByRib(String rib) throws CompteNonExistantException;

    void modifierCompteEmetteure(Compte compteEmetteur, BigDecimal montant);

    void modifierCompteBeneficiaire(Compte CompteBeneficiaire, BigDecimal montant);


}