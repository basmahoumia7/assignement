package ma.octo.assignement.mapper;

import org.springframework.stereotype.Component;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

@Component
public class VirementMapper implements Mapper<Virement, VirementDto> {

	@Override
	public VirementDto map(Virement virement) {
		VirementDto virementDto;
		virementDto = new VirementDto();
		virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
		virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
		virementDto.setDate(virement.getDateExecution());
		virementDto.setMotif(virement.getMotifVirement());
		virementDto.setMontantVirement(virement.getMontantVirement());

		return virementDto;

	}

}
