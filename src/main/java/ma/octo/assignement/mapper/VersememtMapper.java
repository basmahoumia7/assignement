package ma.octo.assignement.mapper;

import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

@Component
public class VersememtMapper implements Mapper<Versement, VersementDto> {

	@Override
	public VersementDto map(Versement versement) {
		VersementDto VersementDto = new VersementDto();

		VersementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
		VersementDto.setRib(versement.getCompteBeneficiaire().getRib());
		VersementDto.setDate(versement.getDateExecution());
		VersementDto.setMotif(versement.getMotifVersement());
		VersementDto.setMontantVersement(versement.getMontantVirement());

		return VersementDto;

	}

}
