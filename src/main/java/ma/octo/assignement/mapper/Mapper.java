package ma.octo.assignement.mapper;

import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;

public interface Mapper<R, T> {

	T map(R r);

	default List<T> map(List<R> models) {
		if (models == null) {
			return null;
		}
		List<T> dtos = new ArrayList<T>();
		for (R r : models) {
			dtos.add(map(r));

		}
      return dtos;
	}

}
